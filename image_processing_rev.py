from numpy import sin, cos, pi
import numpy as np
import matplotlib
matplotlib.use('TKAgg')
import matplotlib.pyplot as plt 
import matplotlib.animation as animation
import scipy.ndimage as ndimage
from scipy import misc
from glob import glob

# global
imgSize = 256
img = np.zeros((imgSize, imgSize))	# a 256 x 256 px image, its
vert = np.zeros((3, 2))	# a 3 x 2 matrix that stores the vertices of a triangle
numEdge = 3 # triangles have 3 edges
offset = 30 # this is used for adding some margin on the figure
img_lists=[] # binary image list
img_obj_list=[] # gray scale image list
fig = plt.figure(figsize=(10, 10))


# ----------------------- Part 1 - Data Generation -----------------------

# generate a random triangle
def generateRandomTriangle(sigma = 0):
	# generate random vertices and mask the img matrix
	generateVertices()
	maskImage()
		

# function that is used to mask the triangle
def maskImage():
	pt = np.zeros((1,2))
	for i in range(0, imgSize):
		for j in range(0, imgSize):
			pt[0, 0] = i
			pt[0, 1] = j
			if isInside(pt):
				img[i, j] = 1
			else:
				img[i, j] = 0


# generate vertices of a triangle
def generateVertices():
	# numbering of the vertices: 0 - on the y axis, 1 - in the 3rd quadrant, 2 - in the 4th quadrant
	# vertex 0 imgSize
	vert[0, 0] = np.random.randint(offset, imgSize - offset) # on the y axis
	vert[0, 1] = imgSize / 2 # at the middle column

	# vertex 1 in the 3rd quadrant
	vert[1, 0] = np.random.randint(imgSize / 2, imgSize - offset)
	vert[1, 1] = np.random.randint(offset, imgSize / 2)

	# vertex 2 in the 4th quadrant
	vert[2, 0] = np.random.randint(offset, imgSize / 2)
	vert[2, 1] = np.random.randint(offset, imgSize / 2)


# decide if a point in the image is inside a triangle
def isInside(point):
	a = np.zeros((1, 2)) # a = p - v0
	b = np.zeros((1, 2)) # b = v1 - v0
	for i in range(0, numEdge):
		a[0, 0] = point[0, 0] - vert[i - 1, 0]
		a[0, 1] = point[0, 1] - vert[i - 1, 1]
		b[0, 0] = vert[i, 0] - vert[i - 1, 0]
		b[0, 1] = vert[i, 1] - vert[i - 1, 1]
		if np.cross(a, b) <= 0:
			return False
	return True


# spin the random triangle and save it
def spinAndSave(rate, time, nimages = 100, prefix = "triangle"):
	rot_img = img
	for i in range(0, nimages):
		img_lists.append(ndimage.rotate(rot_img, rate, reshape=False))
		rot_img = img_lists[-1]
		img_obj_list.append([plt.imshow(rot_img, cmap = plt.cm.gray, interpolation = 'none')])
		misc.imsave("%d.png" % i, rot_img) 
		

# function for geting a list of images
def getImageFiles():
	file_list = glob('*.png')
	file_list.sort()
	return file_list




# ----------------------- Part 2 - Feature Identification -----------------------

# function for getting vertex image using harris
# it works effectively for windowsize between 5 and 7, sigma works at value 2,
# kappa should be kept under 0.08, window space works for 2 and 3
def GetHarrisVertexImage(timg, kappa=0.05, windowSize=5, windowSpace=2, sigma=2):
	Ix = ndimage.sobel(timg, 0)
	Iy = ndimage.sobel(timg, 1)
	gw = np.zeros((windowSize, windowSize))
	center = (windowSize - 1) / 2
	gw[center, center] = 1
	gw = ndimage.gaussian_filter(gw, sigma)
	harrisI = np.zeros(timg.shape)
	for ix in range(center, timg.shape[0] - center, windowSpace):
		for iy in range(center, timg.shape[1] - center, windowSpace):
			Ix_window = Ix[ix - center : ix-center + windowSize, iy - center : iy - center + windowSize]
			Iy_window = Iy[ix - center : ix-center + windowSize, iy - center : iy - center + windowSize]
			Ix2 = np.multiply(Ix_window, Ix_window)
			Ix2_avg = np.sum(np.multiply(Ix2, gw))
			Iy2 = np.multiply(Iy_window, Iy_window)
			Iy2_avg = np.sum(np.multiply(Iy2, gw))
			Ixy = np.multiply(Ix_window, Iy_window)
			Ixy_avg = np.sum(np.multiply(Ixy, gw))
			M = np.array([[Ix2_avg, Ixy_avg], [Ixy_avg, Iy2_avg]])
			harrisI[ix, iy] = np.linalg.det(M) - kappa * (np.trace(M) ** 2)
	return harrisI


# function for getting vertex image using binary filter
def getBinaryVertexmage(timg):
	vertexImg = ndimage.gaussian_filter(timg, 2)
	vertexImg[vertexImg > 0.5] = 1
	vertexImg[vertexImg <= 0.5] = 0
	mask = timg - vertexImg
	return mask.astype(np.float)






# ---------------------------- Auxilliary Functions ------------------------------

# function for animating the images
def animate():
	ani = animation.ArtistAnimation(fig, img_obj_list, interval = 50, blit = False)
	# anim.save('spinning_triangle.mp4', fps=30, extra_args=['-vcodec', 'libx264'])
	plt.show()


# function for showing the image
def showImg(image):
	# plt.figure(figsize = (10, 10))
	plt.imshow(image, cmap = plt.cm.gray, interpolation = 'none')
	plt.axis('off')
	plt.show()

# code for testing
def test():
	# Generate random triangle
	generateRandomTriangle()
	# plt.imshow(img, cmap = plt.cm.gray, interpolation = 'none')
	# plt.show()
	# spinAndSave(360/100, 10)
	# animate()
	# GetHarrisVertexImage(plt.imshow(img, cmap = plt.cm.gray, interpolation = 'none'))
	# hImage = np.zeros(img.shape)
	# hImage[GetHarrisVertexImage(img) > 2] = 1
	# dilated_hImage = ndimage.grey_dilation(hImage, size=(3, 3), structure=np.ones((3, 3)))
	# plt.imshow(dilated_hImage, cmap = plt.cm.gray, interpolation = 'none')
	# plt.show()
	bImage = getBinaryVertexmage(img)
	dilated_bImage = ndimage.grey_dilation(hImage, size=(3, 3), structure=np.ones((3, 3)))
	plt.imshow(dilated_bImage, cmap = plt.cm.gray, interpolation = 'none')
	plt.show()


# main method of the program
def main():
	pass


if __name__ == '__main__':
	# main()
	test()



















