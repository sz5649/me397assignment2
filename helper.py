import scipy.misc as misc
l = misc.lena()
misc.imsave('lena.png', l) 

# np.sum(np.multiply(I(), A))
# sobel filter
# Sx ~= dI/dx = [-1    0    1
#                 -2   0    2
#                 -1     0    1]

# Sy = dI/dy = Sx trans

# gw = ndimage.gauss_filter(delta, sigma)
# check = np.sum(gw) = 1

# M = structure tensor
# M(x,y) = <Ix^2> <IxIy>   -- weighted avg over current window
#          <IxIy> <Iy^2>

# <IxIy> = np.sum(np.multiply(gw, Sx[window]))

# Harris logic:
#  lambda1, 2 = 0
#  lambda1 >0 lambda2 = 0 - edge
#  both > 0, vertex

#  speed up: H(x, y) = det(M) = K*tr^2(M),    K = 0.05, nplnalg.det

#  IH(x, y) = H(x, y)

# # pseudo code
# Ix, Iy, Gw, 

# for ix in (0, len(Ix[0])), space)   ---ix and iy are where the window are centered window is n by n, n is odd, the center c is n-1/2
# 	for iy in (0, len(Ix[0]))
# 		Ix_window = Ix[ix - c, ix-c + n, iy - c; iy - c + n]
# 		1. get blank window
# 		2. calculate M(ix, iy)
# 		3. H(ix, iy) = detM - K*tr^2(M)








