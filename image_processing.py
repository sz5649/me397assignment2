from numpy import sin, cos, pi
import numpy as np
import matplotlib
matplotlib.use('TKAgg')
import matplotlib.pyplot as plt 
import matplotlib.animation as animation
import scipy.ndimage as ndimage
from scipy import misc
from glob import glob
import time

# global
imgSize = 128
img = np.zeros((imgSize, imgSize))	# a 256 x 256 px image, its
vert = np.zeros((3, 2))	# a 3 x 2 matrix that stores the vertices of a triangle
numEdge = 3 # triangles have 3 edges
offset = 20 # this is used for adding some margin on the figure

# image related global var
plt.ion()
fig = plt.figure(figsize=(10, 10))
fig.figimage(img, 250, 250)
# ax = plt.axis([0, 1, 0, 1])
fig.add_axes([0,1,0,1])



# Part 1 - Data Generation

# generate a random triangle
def generateRandomTriangle(sigma = 0):
	# generate random vertices and mask the img matrix
	generateVertices()
	maskImage()
		

# function that is used to mask the triangle
def maskImage():
	pt = np.zeros((1,2))
	for i in range(0, imgSize):
		for j in range(0, imgSize):
			pt[0, 0] = i
			pt[0, 1] = j
			if isInside(pt):
				img[i, j] = 1
			else:
				img[i, j] = 0


# generate vertices of a triangle
def generateVertices():
	# numbering of the vertices: 0 - on the y axis, 1 - in the 3rd quadrant, 2 - in the 4th quadrant
	# vertex 0 imgSize
	vert[0, 0] = np.random.randint(offset, imgSize - offset) # on the y axis
	vert[0, 1] = imgSize / 2 # at the middle column

	# vertex 1 in the 3rd quadrant
	vert[1, 0] = np.random.randint(imgSize / 2, imgSize - offset)
	vert[1, 1] = np.random.randint(offset, imgSize / 2)

	# vertex 2 in the 4th quadrant
	vert[2, 0] = np.random.randint(offset, imgSize / 2)
	vert[2, 1] = np.random.randint(offset, imgSize / 2)


# decide if a point in the image is inside a triangle
def isInside(point):
	a = np.zeros((1, 2)) # a = p - v0
	b = np.zeros((1, 2)) # b = v1 - v0
	for i in range(0, numEdge):
		a[0, 0] = point[0, 0] - vert[i - 1, 0]
		a[0, 1] = point[0, 1] - vert[i - 1, 1]
		b[0, 0] = vert[i, 0] - vert[i - 1, 0]
		b[0, 1] = vert[i, 1] - vert[i - 1, 1]
		if np.cross(a, b) <= 0:
			return False
	return True


# spin the random triangle and save it
def spinAndSave(rate, time, nimages = 100, prefix = "triangle"):
	for i in range(0, nimages):
		# ndimage.rotate(img, 10)
		spin(rate * time)
		# fig.clear()
		# plt.imshow(img, cmap = plt.cm.gray, interpolation = 'none')
		# fig.canvas.draw()
		# TODO animation save function


# spin the triangle onceC
# spin the triangle vertices and mask the triangle
# default the spin direction to be ccw about the center of the image
def spin(ang):
	# ang is specified as deg, need to convert to rad
	theta = ang /180 * pi
	c = cos(theta)
	s = sin(theta)

	# center point coords
	cx = imgSize / 2
	cy = imgSize / 2

	# change the coordinates of vertices
	for i in range(0, numEdge):
		vert[i, 0] -= cx
		vert[i, 1] -= cy
		newX = vert[i, 0] * c - vert[i, 1] * s
		newY = vert[i, 0] * s + vert[i, 1] * c
		vert[i, 0] = newX + cx
		vert[i, 1] = newY + cy

	# mask the new triangle
	maskImage()


# get the image files
def getImageFile(prefix="triangle"):
	pass


# show a static image
def showImg():
	# plt.figure(figsize = (10, 10))
	plt.imshow(img, cmap = plt.cm.gray, interpolation = 'none')
	plt.axis('off')
	plt.show()

# save an image with a name
def saveImg(filename):
	misc.imsave(filename, img)

# function for 
def GetHarrisVertexImage(triangle_image, kappa=0.05, windowSize=9, windowSpace=2, sigma=2):




# code for testing
def test():
	# generate vertex
	# generateVertices()
	# print vert

	# Generate random triangle
	generateRandomTriangle()
	saveImg("triangle.png")
	# showImg()
	# spinAndSave(30, 0.5)


# main method of the program
def main():
	pass


if __name__ == '__main__':
	# main()
	test()


